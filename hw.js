var statGame = {
    countWin: 0,
    countLose: 0,
    countGame: 0
}  

function randomInteger(min, max) {
    let rand = min + Math.random() * (max + 1 - min);
    rand = Math.floor(rand);
    return rand;
  }

var fs = require('fs');  

var readline = require('readline');
var rl = readline.createInterface({
    input: process.stdin, // ввод из стандартного потока
    output: process.stdout // вывод в стандартный поток
});

console.log('Введите 1 или 2. Для выхода введите exit');
rl.on('line', (cmd) => {
    if (cmd === 'exit'){
        rl.close();
        statGame.stat = '----- Итоги игры -----\r\n'+
        'Сыграно '+ statGame.countGame + 'игр, из них:\r\n'+
        'Побед - '+ statGame.countWin+'\r\n'+
        'Поражений - '+ statGame.countLose
        console.log(statGame.stat);

        fs.writeFile("test.txt",statGame.stat, function(err) {
            if (err) throw err;
        });
    }      
    else {
    var ran = randomInteger(1, 2); // случайное 1 или 2    
    console.log('Вы ввели:', cmd);
    console.log('Рандом:', ran); 
    statGame.countGame += 1;
    if (cmd == ran) {
        console.log('Вы УГАДАЛИ !!!');
        statGame.countWin += 1;
    }
        else {
            console.log('Вы НЕ угадали (');
            statGame.countLose += 1;
        }    
    }
});  


